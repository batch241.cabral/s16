let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Sum is: " + sum);

let diff = x - y;
console.log("Difference is: " + diff);

let product = x * y;
console.log("Product is: " + product);

let quotient = x / y;
console.log("Quotient is: " + quotient);

let modulo = y % x;
console.log("Modulo is: " + modulo);

// const oddOrEven = (num) => {
//     result = num % 2;
//     if (result === 0) {
//         return "Even";
//     } else {
//         return "Odd";
//     }
// };
// console.log(oddOrEven(20));

let assignmentNumber = 8;

assignmentNumber += 2;
console.log(assignmentNumber);

assignmentNumber -= 2;
console.log(assignmentNumber);

assignmentNumber *= 2;
console.log(assignmentNumber);

assignmentNumber /= 2;
console.log(assignmentNumber);

const mdas = 1 + 2 - (3 * 4) / 5;
console.log(mdas.toFixed(2));

const mdasVersion2 = 1 + (2 - 3) * (4 / 5);
console.log(mdasVersion2.toFixed(2));

let numA = 12;
let numB = "23";

console.log(numB - numA);
console.log(numB + numA);
